from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

#lists all todos
def todo_list_list(request):
    all_todos = TodoList.objects.all()
    context = {
        "all_todos": all_todos,
    }
    return render(request, "todos/list.html", context)

#shows one todo list
def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "details": details,
    }
    return render(request, "todos/details.html", context)

#creates a todo list
def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

#creates an item in a todo list
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/item_create.html", context)

#update an item
def todo_item_update(request, id):
    item_object = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item_object)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item_object)
    context = {
        "form": form,
    }
    return render(request, "todos/item_update.html", context)


#updates a todo list
def todo_list_update(request, id):
    details = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=details)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = TodoForm(instance=details)

    context = {
        "details": details,
        "form": form,
    }
    return render(request, "todos/edit.html", context)

#deletes a todo list
def todo_list_delete(request,id):
    details = TodoList.objects.get(id=id)
    if request.method == "POST":
        details.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
